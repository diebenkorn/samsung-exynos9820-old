#!/bin/bash
export ARCH=arm64
export SUBARCH=arm64
export ANDROID_MAJOR_VERSION=q
export CLANG_PATH=/home/hades/Desktop/toolchains/google/clang-r365631b/bin
export CLANG_TRIPLE=aarch64-linux-gnu-
export CROSS_COMPILE=/home/hades/Desktop/toolchains/google/aarch64-linux-android-4.x/bin/aarch64-linux-android-
export PATH=${CLANG_PATH}:${PATH}
make CC=clang clean
make CC=clang ARCH=arm64 exynos9820-beyond2lte_defconfig
make CC=clang ARCH=arm64 -j128

